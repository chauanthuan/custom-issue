<?php
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class HappyBirthdayServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views/HappyBirthday', 'HappyBirthday');
    }
    
}