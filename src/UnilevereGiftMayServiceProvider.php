<?php
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class UnilevereGiftMayServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views/Unilever_eGift_May', 'Unilever_eGift_May');
    }
    
}