<!DOCTYPE html>
<html lang="{{ Cookie::get('laravel_language','vi') }}">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" content="">
    <meta name="format-detection" content="telephone=no">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!!  csrf_token()  !!}" />
    <meta http-equiv="content-language" content="{!!  Cookie::get('laravel_language','vi')  !!}">
    <title>{{trans('content.gotit_title')}}</title>
    <link rel="shortcut icon" href="/favicon.gif">
    <!-- Bootstrap core CSS -->
    <link href="{!!  asset('layouts/v4/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!!  asset('layouts/v4/css/bootstrap-select.min.css') !!}" rel="stylesheet">
    <link href="{!!  asset('layouts/v4/fonts/stylesheet.css') !!}" rel="stylesheet">
    <link href="{!!  asset('layouts/v4/css/icomoonstyle.css') !!}" rel="stylesheet">
    <link href="{!!  asset('layouts/v4/css/custom.css') !!}" rel="stylesheet">
    <?php
        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
        $langPos = strpos($browserLang, 'vi');

        if($langPos === false){
            $browserLang = 'en';
        }else{
            $browserLang = 'vi';
        }

        $lang = Cookie::get('laravel_language', $browserLang);
        $ip = $_SERVER['REMOTE_ADDR'];
    ?>
  </head>

  <body id="page-top">
    <?php //dd($list_brand_id);?>
    
    <div class="list_menu">
      <ul>
        <li><a href="https://www.gotit.vn/egift#faqs">{{trans('content.how_use_vc')}}</a></li>
        <li><a href="https://www.gotit.vn/faq.html">{{trans('content.faq_v')}}</a></li>
        <li><a href="https://www.gotit.vn/product.html">{{trans('content.popular_gift_v')}}</a></li>
        <li><a href="https://biz.gotit.vn/">{{trans('content.gotit_for_bussiness')}}<small>www.biz.gotit.vn</small></a></li>
        <li><a href="https://www.gotit.vn/">{{trans('content.what_is_gotit')}}<small>www.gotit.vn</small></a></li>
        <li><a href="https://www.facebook.com/quatangGotIt/">{{trans('content.follow_us')}}<small>facebook.com/gotit.vn</small></a></li>
        <li><a href="https://brand.gotit.vn/">{{trans('content.all_brands')}}<small>brand.gotit.vn</small></a></li>
        
        <li class="hotline_menu">Got It Hotline: <b>1900 5588 20</b></li>
      </ul>
    </div>
    <section id="voucherWrapper" class="welcome_page active" style="transition: 0.3s; left: 0px; background-image: url('https://img.gotit.vn/kone.jpg'); right: 0px;">
      <div class="fixed_btn voucher_footer">
        <div class="footer_info footer_city" style="background-color:{{($voucher_tp != null && $voucher_tp->customer_color != null ? $voucher_tp->customer_color : '#ff4e4e')}}">
          <a href="javascript:void(0)" class="open_gift_btn">Mở quà của bạn</a>
        </div>
      </div>
    </section>
    <section class="main_page" style="right:-100%;left:initial;position: fixed;">
      <div class="navbar-top">
        <div class="side-nav-panel-left">
          <a href="#" data-activates="slide-out-left" class="side-nav-left menu_icon">
            <span class=""></span></a>
        </div>

        <div class="site-brand">
          <a href="index.html"><img class="logo-top" src="{{($voucher_tp != null && $voucher_tp->customer_logo != null ? env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->customer_logo : '../layouts/v4/img/logo.png')}}"></a>
        </div>

        <div class="side-nav-panel-right">
          <a href="javascript:void(0)" class="btn-language" data-lang="{{$lang}}" href="javascript:void(0)"><span class="icon-language"></span><small>{{$lang == "vi" ? "en":"vn"}}</small></a>
        </div>
      </div>
      <section class="info-voucher text-center" id="info-voucher">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 mx-auto wrap">
              <h2 class="section-heading" style="background-color:{{($voucher_tp != null && $voucher_tp->customer_color != null ? $voucher_tp->customer_color : '#ff4e4e')}}">{{$psize}}</h2>
              <p>{!! trans('content.show_code_to_staffv4') !!}</p>
              <div class="code-wrap">
                  <img src="../layouts/v4/img/logo.png" alt="Got It" class="logo_gotit">
                  <p class="barcode">
                      <img src="<?php echo '/voucher/barcode/'.$voucher->code?>" />
                      <span class="code">{{$voucher->code}}</span>
                  </p>
                  <p class="validate">{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
              </div>
              <div class="save-voucher">
                  <a href="#" class="save_btn">Add to your voucher</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="info-brand" id="info-brand">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 mx-auto wrap">
              @if(!empty($listCity))
                <select class="selectpicker city-select" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.filter_by_location')}}" data-width="100%"  data-size="8">
                  <option value="0">{{trans('content.all')}}</option>
                  @foreach($listCity as $item)
                    @if(count($item) > 0)
                      <option value="{{$item[0]->city_id}}">
                        {{Translate::transObj($item[0],'city_name')}}
                      </option>
                    @endif
                  @endforeach
                </select>
              @endif
              <div class="listCateBrand">
                <div class="list-brand popular">
                  <h2 class="heading">{{trans('content.popular_brand')}}</h2>
                  <div class="list-item">
                  @foreach($list_brand as $brandItem)
                    @if($brandItem->is_popular == 1)
                      <a href="javascript:void(0)" class="brand_wr {{($brandItem->brand_img_guide) ? 'has_guide' : ''}}" data-brandid="{{$brandItem->brand_id}}" data-brand-guide="{{($brandItem->brand_img_guide) ? $brandItem->brand_img_guide : ''}}" data-weburl="{{($brandItem->web_url) ? $brandItem->web_url : ''}}">
                        <img class="logo-brand" src="{{env('IMG_SERVER').$brandItem->img_path}}" alt="{{($lang == 'vi' ? $brandItem->brand_name_vi : $brandItem->brand_name_en)}}">
                      </a>
                    @endif
                  @endforeach
                  </div>
                </div>
                @if(!empty($listCategory))
                  @foreach($listCategory as $category)
                    @if(count($category) > 0)
                      <div class="list-brand">
                        <h2 class="heading">{{Translate::transObj($category[0],'cate_name')}}</h2>
                        <div class="list-item">
                          @foreach($category as $brandItem)
                          <a href="javascript:void(0)" class="brand_wr {{($brandItem->brand_img_guide) ? 'has_guide' : ''}}" data-brandid="{{$brandItem->brand_id}}" data-brand-guide="{{($brandItem->brand_img_guide) ? $brandItem->brand_img_guide : ''}}" data-weburl="{{($brandItem->web_url) ? $brandItem->web_url : ''}}">
                            <img class="logo-brand" src="{{env('IMG_SERVER').$brandItem->brand_img_path}}" alt="{{Translate::transObj($brandItem,'brand_name')}}">
                          </a>
                          @endforeach
                        </div>
                      </div>
                    @endif
                  @endforeach
                @endif
              </div>
              <div class="list-brand topup">
                <h2 class="heading">{{trans('content.topup')}}</h2>
                <div class="list-item">
                  <a href="javascript:void(0)" class="brand_wr">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921408_KxS6U.png" alt="Viettel">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471920885_6tISg.png" alt="Mobilephone">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921291_yINyr.png" alt="Vinaphone">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr">
                    <img class="logo-brand" src="../layouts/v4/img/vietnamoblie.png" alt="Vietnammobi">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr">
                    <img class="logo-brand" src="../layouts/v4/img/Gmobile.png" alt="Gmobi">
                  </a>
                </div>
              </div>
              <!-- <div  class="showmore" style="">
                 <a href="#" id="loadMore">{{trans('content.viewmore_category',['number'=>3])}}</a>
                 <a href="#" id="hideMore" style="display: none">{{trans('content.viewless')}}</a>
              </div> -->
              <div class="note-brand">
                <p>{{trans('content.note_brand_accept')}}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section class="footer-info">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 wrap">
              <p class="how-use"><a href="https://www.gotit.vn/egift#faqs">{{trans('content.how_use_this_vc')}}</a></p>
              <p class="hotline">Got It Hotline: 1900 5588 20</p>
              <p class="view-nearest"><a href="javascript:void(0)" style="background-color:{{($voucher_tp != null && $voucher_tp->customer_color != null ? $voucher_tp->customer_color : '#ff4e4e')}}">{{trans('content.view_nearest_store')}}</a></p>
            </div>
          </div>
        </div>
      </section>
    </section>
    <section id="brand-detail" class="brand-detail" style="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 wrap">
            <h2 class="heading"><span class="close-brand-detail"></span>Brand Details</h2>
            <div class="brand-detail-info">
              <img src="">
              <h3 class="brand-name"></h3>
              <p class="short-desc"></p>
           
            </div>
            <div class="store-info">
              <div class="top-info">
                <h2 class="float-left"><?php echo trans('content.stores')?></h2>
                <span class="float-right"></span>
              </div>
              <select class="selectpicker city-brandDetail" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.filter_by_location')}}" data-width="100%"  data-size="8">
                
              </select>
              <ul class="list-store">
                 
              </ul>
              <div  class="showmore" style="">
                 <a href="#" id="loadMore" style="display: none;">View more stores</a>
                 <a href="#" id="hideMore" style="display: none">View less</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="nearest-detail" class="nearest-detail" style="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 wrap">
            <h2 class="heading"><span class="close-nearest-detail"></span>{{trans('content.nearest_store')}}</h2>
            <p class="sub-title">{{trans('content.filtered_within_km')}}</p>
            
            <div class="store-info">
              <div class="top-info">
                <h2 class="float-left">{{trans('content.stores')}}</h2>
                <span class="float-right"></span>
              </div>

              @if(!empty($listCategory))
                <select class="selectpicker category-store-select" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.all_category')}}" data-width="100%"  data-size="8">
                  @foreach($listCategory as $category)
                    @if(count($category) > 0)
                      <option value="{{$category[0]->category_id}}">
                        {{Translate::transObj($category[0],'cate_name')}}
                      </option>
                    @endif
                  @endforeach
                </select>
              @endif
              <ul class="list-store">
                 
              </ul>
              <!-- <div  class="showmore" style="">
                 <a href="#" id="loadMore">View more 3 stores</a>
                 <a href="#" id="hideMore" style="display: none">View less</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Topup modal -->
    <div id="topupModal" class="modal topupModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
          <div class="modal-body">
            <img src="https://img.gotit.vn/group_phone2.png">
            <div class="error_phone" style="color:#ff5f5f;text-align: left;display:none"></div>
            <div class="alert alert-success topup-success" style="display:none">
              <strong>Success</strong>
            </div>
            <p>{{trans('content.note_topup')}}</p>
            <input type="text" name="phone_number" value="" class="form-control phone_number" placeholder="{{trans('content.phone_number')}}">
            <input type="hidden" value="{{$voucher->code}}" name="code">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn topupBtn">{{trans('content.topup_now')}}</button>
            <button type="button" class="btn btn-close" data-dismiss="modal"><span class="icon-close"></span>{{trans('content.close')}}</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Guide modal -->
    <div id="guideModal" class="modal guideModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
          <div class="modal-body">
            <img src="">
            <input type="hidden" value="{{$voucher->code}}" name="code">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn guideBtn">{{trans('content.guide_btn_content')}}</button>
            <button type="button" class="btn btn-close" data-dismiss="modal"><span class="icon-close"></span>{{trans('content.close')}}</button>
          </div>
        </div>
      </div>
    </div>
    <div class="loading">
      <div id='img-loading' class='uil-spin-css' style="-webkit-transform:scale(0.4)"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
    </div>

    <input name="store_group_id" type="hidden" value="{{$product->store_group_id}}"/>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="img_server" value="{{env('IMG_SERVER')}}">
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{!!  asset('layouts/v4/js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('layouts/v4/js/bootstrap.bundle.min.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('layouts/v4/js/bootstrap-select.min.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('layouts/v4/js/jquery.easing.min.js') !!}"></script>
    <script type="text/javascript" src="{!!  asset('layouts/v4/js/jquery.show-more.js') !!}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
  
    <!-- Custom scripts for this template -->
    <!-- <script src="js/new-age.min.js"></script> -->
    <script type="text/javascript">
      var lang = $('html').attr('lang');
      var img_serve = $('input[name="img_server"]').val();
      var locations = [];
      var typeFind = '';
      var brandIdChoose = '';
      var store_group_id = $('input[name="store_group_id"]').val();
       //loadmore brand detail
      var size_listItem_brand = 0;
      var weburlRidirect = '';
      var x=20;
      <?php
        $ListBrandId = array();
        foreach ($list_brand_id as $key => $value) {
          $ListBrandId[$key] =  $value;
        }
       echo 'var list_brand_id = '.json_encode($ListBrandId).';';
      ?>
      
      $(document).ready(function(){
        //getlistCategoryBrandByCity
        // $(".info-brand .list-brand").slice(0, 3).show();
        // if ($(".info-brand .list-brand:hidden").length != 0) {
        //   $(".info-brand #loadMore").show();
        // }   
        // $(".info-brand #loadMore").on('click', function (e) {
        //   e.preventDefault();
        //   $(".info-brand .list-brand:hidden").slice(0, 3).slideDown();
        //   if ($(".info-brand .list-brand:hidden").length == 0) {
        //     $(".info-brand #loadMore").hide();
        //     $(".info-brand #hideMore").show();
        //   }
        // });
        // $(".info-brand #hideMore").on('click', function (e) {
        //   e.preventDefault();
        //   $(".info-brand .list-brand").slice(3, 6).slideUp();
        //     $(".info-brand #loadMore").show();
        //     $(".info-brand #hideMore").hide();
        // });

        $('.brand-detail #loadMore').click(function () {
            x= (x+10 <= size_listItem_brand) ? x+10 : size_listItem_brand;
            $('.brand-detail .list-store li:lt('+x+')').show();

            if (x==size_listItem_brand) {
              $('.brand-detail .store-info .showmore').hide();
              $(".brand-detail #loadMore").hide();
            } 
        });
        


        $('.menu_icon').click(function(e){
          e.preventDefault();
          $(this).toggleClass('showMenu');
          var wid = $(window).width();
          var left = (wid - $('.main_page').width())/2;
          console.log(left);
          $('body').toggleClass('showMenu');
          $('body').css({'position':'fixed'});
          $('.list_menu').toggleClass('showMenu');
          if($('.list_menu').hasClass('showMenu')){
            
            $('.list_menu').css({'width':(left+280)+'px'});
          }
          else{
            $('body').css({'position':'relative'});
            $('.list_menu').css({'width':280+'px'});
          }
          
          // $('body').css({'left':(left)+'px'});
        });
        
        $(".list-brand.topup .list-item .brand_wr").click(function(){
          $('.error_phone').text('');
          $('#topupModal').modal({
            'show':true,
            'backdrop':'static'
          })
        });

        
        $('.short-desc').showMore({
            minheight: 35,
            buttontxtmore: (lang == 'vi' ? 'Xem thêm':'View more'),
            buttontxtless: (lang == 'vi' ? 'Ẩn bớt':'View less'),
            animationspeed: 250,
            buttoncss: 'viewmoreBrand'
        });

        $('body').on('click','.info-brand .list-brand:not(".topup") .brand_wr:not(".has_guide")',function(e){
          e.preventDefault();
          $('.loading').css({'display':'block'});
          $('.brand-detail #loadMore').hide();
          $('.brand-detail .store-info .showmore').hide();
          typeFind="filterBrand";
          
          var brand_id = $(this).data().brandid;
          brandIdChoose = parseInt(brand_id);
          var city_id = $('.selectpicker.city-select').val();
          $(".brand-detail ul.list-store").empty();
          $('.brand-name').text('');
          $('.short-desc').text('');
          
          //get list city by brand
          setTimeout(function(){
            $.when(
              $.ajax({
                url: "/voucher/getstore",
                type: 'POST',
                data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id},
                dataType: 'json',
                async:false,
                success: function(data){
                  locations = [];

                  $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                  $('.brand-detail-info .brand-name').text(lang == 'vi' ? data[0].brand_name_vi : data[0].brand_name_vi);
                  $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                  $.each(data, function(index, storeObj){
                    var store = {
                      'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                      'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                      'lat' : storeObj.lat,
                      'long' : storeObj.long,
                      'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                      'phone' : storeObj.phone,
                    }
                    locations.push(store);
                  });
                }
              }),
              $.ajax({
                url:'/voucher/getCityByBrand',
                type:'POST',
                data:{brand_id:brand_id},
                dataType:'json',
                async:false,
                success:function(response){
                   //add city in dropdown filter
                    $('select.city-brandDetail').empty();
                    $('select.city-brandDetail').append('<option value="0"><?php echo trans("content.all")?></option>')
                    $.each(response['list_city'],function(index,city){
                        if(lang == 'vi'){
                            $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_vi+'</option>');
                        }
                        else{
                            $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_en+'</option>');
                        }

                    }); 
                    $('select.city-brandDetail').selectpicker('refresh');
                    if(city_id){
                      $('select.city-brandDetail').selectpicker('val', city_id);
                    }
                    
                }
              }),
              //get list store theo brand va storegroupid
              
            ).then(function() {
              $('.brand-detail .top-info span').text(locations.length +' <?php echo trans("content.stores")?>');
              FindNear();
              setTimeout(function(){
                size_listItem_brand =  $(".brand-detail .list-store li").length; 
                if (size_listItem_brand >= x) {
                  $(".brand-detail #loadMore").show();
                  $('.brand-detail .store-info .showmore').show();
                }
                
                $('.brand-detail .list-store li:gt('+x+')').hide();

                
              },200);
              $('.loading').css({'display':'none'});
              $('body').css({'position':'fixed'});
              var wid = $('body').width();
              var right = (wid - $('.brand-detail').width())/2;
              $('.main_page').css({'right':'100%', '-webkit-transition':'0.3','-moz-transition':'0.3','transition':'0.3'});
              $('.brand-detail').css({'right':right, '-webkit-transition':'0.3','-moz-transition':'0.3','transition':'0.3'});
              
              $('.short-desc').showMore({
                minheight: 35,
                buttontxtmore: (lang == 'vi' ? 'Xem thêm':'View more'),
                buttontxtless: (lang == 'vi' ? 'Ẩn bớt' : 'View less'),
                animationspeed: 250,
                buttoncss: 'viewmoreBrand'
              });
            })
          },800);
          
          
        });

        $('body').on('click','.info-brand .list-brand:not(".topup") .brand_wr.has_guide',function(e){
          var image_guide = $(this).data('brand-guide');
          var weburl = $(this).data('weburl');
          var brand_id = $(this).data('brandid');
          if(weburl && weburl != ''){
              if(weburl.indexOf('{code}') >= 0){
                weburl = weburl.replace("{code}", "<?php echo $voucher->code?>");
              }
              if(weburl.indexOf('{link}') >= 0){
                weburl = weburl.replace("{link}", "<?php echo $voucher->link?>");
              }
              if(weburl.indexOf('{date}') >= 0){
                weburl = weburl.replace("{date}", "<?php echo $voucher->expired_date?>");
              }
            
            weburlRidirect = weburl;
          }
          $('#guideModal .modal-footer .guideBtn').attr('data-brandid', brand_id);
          $('#guideModal .modal-body img').prop('src',img_serve+''+image_guide);
          // setTimeout(function(){
            $('#guideModal').modal({
              'show':true,
              'backdrop':'static'
            })
          // },1000);
        });

        $("body").on('click','.guideModal .guideBtn', function(e){
          if(weburlRidirect != ''){
            window.open(weburlRidirect, "_blank");
          }
          else{
            e.preventDefault();
            $('#guideModal').modal('hide');
            $('.loading').css({'display':'block'});
            $('.brand-detail #loadMore').hide();
            $('.brand-detail .store-info .showmore').hide();
            typeFind="filterBrand";
            
            var brand_id = $(this).data().brandid;
            brandIdChoose = parseInt(brand_id);
            var city_id = $('.selectpicker.city-select').val();
            $(".brand-detail ul.list-store").empty();
            $('.brand-name').text('');
            $('.short-desc').text('');
            
            //get list city by brand
            setTimeout(function(){
              $.when(
                $.ajax({
                  url: "/voucher/getstore",
                  type: 'POST',
                  data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id},
                  dataType: 'json',
                  async:false,
                  success: function(data){
                    locations = [];

                    $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                    $('.brand-detail-info .brand-name').text(lang == 'vi' ? data[0].brand_name_vi : data[0].brand_name_vi);
                    $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                    $.each(data, function(index, storeObj){
                      var store = {
                        'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                        'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                        'lat' : storeObj.lat,
                        'long' : storeObj.long,
                        'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                        'phone' : storeObj.phone,
                      }
                      locations.push(store);
                    });
                  }
                }),
                $.ajax({
                  url:'/voucher/getCityByBrand',
                  type:'POST',
                  data:{brand_id:brand_id},
                  dataType:'json',
                  async:false,
                  success:function(response){
                     //add city in dropdown filter
                      $('select.city-brandDetail').empty();
                      $.each(response['list_city'],function(index,city){
                          if(lang == 'vi'){
                              $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_vi+'</option>');
                          }
                          else{
                              $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_en+'</option>');
                          }

                      }); 
                      $('select.city-brandDetail').selectpicker('refresh');
                      if(city_id){
                        $('select.city-brandDetail').selectpicker('val', city_id);
                      }
                      
                  }
                }),
                //get list store theo brand va storegroupid
                
              ).then(function() {
                $('.brand-detail .top-info span').text(locations.length +' <?php echo trans("content.stores")?>');
                FindNear();
                setTimeout(function(){
                  size_listItem_brand =  $(".brand-detail .list-store li").length; 
                  if (size_listItem_brand >= x) {
                    $(".brand-detail #loadMore").show();
                    $('.brand-detail .store-info .showmore').show();
                  }
                  
                  $('.brand-detail .list-store li:gt('+x+')').hide();

                  
                },200);
                $('.loading').css({'display':'none'});
                $('body').css({'position':'fixed'});
                var wid = $('body').width();
                var right = (wid - $('.brand-detail').width())/2;
                $('.brand-detail').css({'right':0, '-webkit-transition':'0.3','-moz-transition':'0.3','transition':'0.3'});
                
                $('.short-desc').showMore({
                  minheight: 35,
                  buttontxtmore: (lang == 'vi' ? 'Xem thêm':'View more'),
                  buttontxtless: (lang == 'vi' ? 'Ẩn bớt' : 'View less'),
                  animationspeed: 250,
                  buttoncss: 'viewmoreBrand'
                });
              })
            },800);
          }
        });

        //select box city in brand detail change
        $('.selectpicker.city-brandDetail').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
          $('.brand-detail #loadMore').hide();
          var city_id = $(e.currentTarget).val();
          var brand_id = brandIdChoose;
          
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            $.ajax({
              url: "/voucher/getstore",
              type: 'POST',
              data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id},
              dataType: 'json',
              async:false,
              success: function(data){
                locations = [];
                // console.log(data);
                $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                $.each(data, function(index, storeObj){
                  var store = {
                    'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                    'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                    'lat' : storeObj.lat,
                    'long' : storeObj.long,
                    'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                    'phone' : storeObj.phone,
                  }
                  locations.push(store);
                });
              }
            }).done(function(){
              $('.loading').css({'display':'none'});
              $('.brand-detail .top-info span').text(locations.length +' <?php echo trans("content.stores")?>');
              FindNear();
              setTimeout(function(){ 
                size_listItem_brand =  $(".brand-detail .list-store li").length;
                if (size_listItem_brand >= x) {
                  $(".brand-detail #loadMore").show();
                }
                $('.brand-detail .list-store li:gt('+x+')').hide();
              },200);
            })
          },800);
        });

        $('.close-brand-detail').click(function(){
          typeFind = '';
          $('.main_page').css({'right':0, '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('.brand-detail').css({'right':'-100%', '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('.brand-detail .store-info .showmore').show();
          $(".brand-detail #loadMore").show();
          $('body').css({'position':'relative'});
        });

        $('.view-nearest a').click(function(){
          typeFind="filterNearest";
          <?php
            $storeArr = array();
            foreach ($stores as $store) {
            $storeArr[] = array(
            'storeNm'  => Translate::transObj($store, 'name'),
            'brandNm'  => Translate::transObj($store, 'brand_name'),
            'lat'   => $store->lat,
            'long'   => $store->long,
            'storeAddr'   =>  Translate::transObj($store, 'address'),
            'phone' => $store->phone
            );
            }
            echo 'var listStore = '.json_encode($storeArr).';';
          ?>

          $('.nearest-detail .top-info span').text(listStore.length +' <?php echo trans('content.stores')?>');
          locations = listStore;
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            FindNear();
            $('.loading').css({'display':'none'});
            $('body').css({'position':'fixed'});
            var wid = $('body').width();
            var right = (wid - $('.brand-detail').width())/2;
            $('.main_page').css({'right':'100%', '-webkit-transition':'0.3','-moz-transition':'0.3','transition':'0.3'});
            $('.nearest-detail').css({'right':right,'-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
            
          },300); 

        });

        // getcatebrandbycity
        $('.selectpicker.city-select').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {

          var city_id = $(e.currentTarget).val();

          var listBrandId = list_brand_id;
          
          var countCate = 0;
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            $.ajax({
              url:'/voucher/getcatebrandbycity',
              method:'POST',
              data:{city_id:city_id,list_brand_id:list_brand_id},
              async:false,
              success:function(response){

                if(response){
                  $('.info-brand .list-brand').not('.popular,.topup').remove();
                  countCate = Object.keys(response.listCateBrand).length;
                  $('.list-brand.popular .list-item a').css({'display':'none'});
                  
                  $.each(response.listCateBrand, function(index, value){ 
                    
                    var html = '';
                    
                    html += '<div class="list-brand">';
                    html += '<h2 class="heading">'+(lang == 'vi' ? value[0].cate_name_vi : value[0].cate_name_en ) +'</h2>';
                    html += '<div class="list-item">';
                    $.each(value, function(i, item){
                      console.log(item);
                      $('.list-brand.popular .list-item').find('a[data-brandid="'+item.brand_id+'"]').css({'display':'inline-block'});
                      html += '<a href="javascript:void(0)" class="brand_wr" data-brandid='+item.brand_id+' data-brand-guide="'+(item.brand_img_guide ? item.brand_img_guide : '')+'" data-weburl="'+(item.web_url ? item.web_url : '')+'" >'+
                        '<img class="logo-brand" src="https://img.gotit.vn/'+item.brand_img_path+'"/>'+
                      '</a>';
                    });
                    html += '</div>';
                    html += '</div>';
                    $('.info-brand .listCateBrand').append(html);
                  })  
                
                  // setTimeout(function(){
                  //   if ($(".brand_body.popular .list_item a.brand_wr:visible").length == 0) {
                  //       $(".brand_body.popular").css({'visibility':'hidden','height':0});
                  //   }
                  //   else {
                  //       $(".brand_body.popular").css({'visibility':'visible','height':'auto'});
                  //   }
                  // },100);                
                  setTimeout(function(){
                    if ($(".info-brand .list-brand.popular .list-item a:visible").length == 0) {
                        $(".info-brand .list-brand.popular").css({'visibility':'hidden','height':0,'margin-top':'0px','padding-bottom':'0px'});
                    }
                    else {
                        $(".info-brand .list-brand.popular").css({'visibility':'visible','height':'auto','margin-top':'20px','padding-bottom':'10px'});
                    }
                  },100);
                }
              },
            }).done(function(){
              $('.loading').css({'display':'none'});
              // $(".info-brand #hideMore").hide();
              // $(".info-brand .list-brand").slice(0, 3).show();
              // if ($(".info-brand .list-brand:hidden").length != 0) {
              //   $(".info-brand #loadMore").text((lang == "vi" ? "Xem thêm "+(countCate - 2)+" thể loại" : "View more "+(countCate - 2)+" categories"));
              //   $(".info-brand #loadMore").show();
              // }   
              // else{
              //   // $(".info-brand .showmore").hide();
              //   $(".info-brand #loadMore").hide();
              // }
            });
          },1000);
        });

        //select box category in store detail change
        $('.selectpicker.category-store-select').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
          var category_id = $(e.currentTarget).val();
          console.log(category_id);
          var brand_id = brandIdChoose;
          
          var city_id = 0;
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            $.when(
              $.ajax({
                url:'/voucher/getbrand',
                method:'POST',
                data:{cate_id:category_id,store_group_id:store_group_id},
                async:false,
                success:function(response){
                  //call list brand -> trong do call từng brand ajax
                 
                  if(response['list_brand'].length > 0){
                   
                    locations = [];
                    $.each(response['list_brand'],(index,brand)=>{
                      var brand_id_item = brand.brand_id;
                      $.ajax({
                        url: "/voucher/getstore",
                        type: 'POST',
                        data: {brand_id: brand_id_item,store_group_id:store_group_id,city_id:city_id},
                        dataType: 'json',
                        async:false,
                        success: function(data){
                         
                          $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                          $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                          $.each(data, (index, storeObj) =>{
                            var store = {
                              'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                              'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                              'lat' : storeObj.lat,
                              'long' : storeObj.long,
                              'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                              'phone' : storeObj.phone,
                            }
                            locations.push(store);
                          });
                        }
                      })
                    });
                  }
                },
              })
            ).then(function(){
              $('.loading').css({'display':'none'});
              $('.nearest-detail .top-info span').text(locations.length +' <?php echo trans('content.stores')?>');
              FindNear();
            })
          },1000);
          // $.ajax({
          //   url:'/voucher/getbrand',
          //   method:'POST',
          //   data:{cate_id:category_id,store_group_id:store_group_id},
          //   async:false,
          //   success:function(response){
          //     //call list brand -> trong do call từng brand ajax
             
          //     if(response['list_brand'].length > 0){
               
          //       locations = [];
          //       $.each(response['list_brand'],(index,brand)=>{
          //         var brand_id_item = brand.brand_id;
          //         $.ajax({
          //           url: "/voucher/getstore",
          //           type: 'POST',
          //           data: {brand_id: brand_id_item,store_group_id:store_group_id,city_id:city_id},
          //           dataType: 'json',
          //           async:false,
          //           success: function(data){
                     
          //             $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
          //             $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
          //             $.each(data, (index, storeObj) =>{
          //               var store = {
          //                 'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
          //                 'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
          //                 'lat' : storeObj.lat,
          //                 'long' : storeObj.long,
          //                 'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
          //                 'phone' : storeObj.phone,
          //               }
          //               locations.push(store);
          //             });
          //           }
          //         })
          //       });
          //     }
          //   },
          // }).done(function(){
          //   $('.loading').css({'display':'none'});
          //   $('.nearest-detail .top-info span').text(locations.length +' <?php echo trans('content.stores')?>');
          //   FindNear();
          // })
        });

        $('.close-nearest-detail').click(function(){
          typeFind = '';
          $('.main_page').css({'right':0, '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('.nearest-detail').css({'right':'-100%', '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('body').css({'position':'relative'});
        });

        //topup
        $('.topupBtn').click(function(){
          $('.topup-success').hide();
          
          var phone_val = $('input[name="phone_number"]').val();
          //var amount = $('input[name="price_value"]').val();
          //var amount = '10000';
          var code = $('input[name="code"]').val();

          var err = '';
          if(phone_val.length == 0){
            if(lang == 'vi'){
                err += '<p>Vui lòng nhập số điện thoại</p>';
            }
            else{
                err += '<p>Please enter the phone number</p>';
            } 
          }
          else if(phone_val.length > 11 || phone_val.length < 10){
            if(lang == 'vi'){
                err += '<p>Số điện thoại không được ít hơn 10 số hoặc nhiều hơn 11 số</p>';
            }
            else{
                err += '<p>Phone numbers must not be less than 10 digits or more than 11 digits</p>';
            }
              
          }
          else{
            var regExp = /^(09|08|01[2689])\d{8}$/; 
            var checkphone = phone_val.match(regExp);
            if (!checkphone) {
                if(lang == 'vi'){
                    err += '<p>Số điện thoại không hợp lệ, vui lòng kiểm tra lại</p>';
                }
                else{
                    err += '<p>Phone number is invalid, please check again</p>';
                }
            }
          }

          if(err != '' || err.length > 0){
            $('.error_phone').html('').append(err).css({'display':'block'});
            return false;
          }
          else{
            $('.error_phone').html('');
            $('.loading').css({'display':'block'});
            // $('#img-loading').show();
            var GotitRequest = {
                phoneNumber:phone_val,code:code
            };

            var hasResult = false;

            $.ajax({
              url:'<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/topup',//call lumen project
              async: true,
              crossDomain: true,
              type:'POST',
              dataType:'json',
              contentType: "application/json; charset=utf-8",
              headers: {
                  "Content-Type": "application/json"
              },
              data: JSON.stringify(GotitRequest),
              timeout:30000,
              success:function(data){
                var msg = '';
                if(lang == 'vi'){
                    if(data.stt == -2 || data.stt == -1 || data.stt == -3){
                        msg = 'Voucher không hợp lệ';
                    }
                    else if(data.stt == 0){
                        msg = 'Voucher đã sử dụng';
                    }
                    else if(data.stt == 1){
                        msg = 'Nạp tiền thành công';
                    }
                    else if(data.stt == 3){
                        msg = 'Nạp tiền thất bại';
                    }
                    else if(data.stt == 2){
                        msg = 'Đang xử lý quá trình nạp tiền';
                    }
                    else if(data.stt == 4){
                        msg = 'Quá nhiều yêu cầu từ máy bạn. Vui lòng tải lại trang và thử lại sau khoảng 1 phút.';
                    }
                    else if(data.stt == 5){
                        msg = 'Giao dịch top-up thất bại do lỗi từ telco: SỐ ĐT KHÔNG HỢP LỆ.';
                    }
                    else if(data.stt == 6){
                        msg = 'Thuê bao không có tài khoản EZPay (trường hợp của Vinaphone).';
                    }
                    else if(data.stt == 7){
                        msg = 'Thuê bao đang bị khóa chiều nạp.';
                    }
                }
                else{
                    if(data.stt == -2 || data.stt == -1){
                        msg = 'Voucher Invalid';
                    }
                    else if(data.stt == 0){
                        msg = 'Voucher has used';
                    }
                    else if(data.stt == 1){
                        msg = 'Topup card success';
                    }
                    else if(data.stt == 3){
                        msg = 'Topup card fail';
                    }
                    else if(data.stt == 2){
                        msg = 'Topup card processing';
                    }
                    else if(data.stt == 4){
                        msg = 'Too many request, please refresh page and try again in a minuste.';
                    }
                    else if(data.stt == 5){
                        msg = 'Topup failed: Phone number is not valid';
                    }
                    else if(data.stt == 6){
                        msg = 'This phone number has no EZPay account (Vinaphone).';
                    }
                    else if(data.stt == 7){
                        msg = 'Phone number is blocked to topup.';
                    }
                }
                
             $('.topup-success strong').text(msg);
              $('.topup-success').show();

                if(data.stt != 2){
                    hasResult = true;
                }
              },

              error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(ajaxOptions);
                console.log(thrownError);

                if(ajaxOptions==="timeout") {
                    $('.loading').css({'display':'none'});

                    if(lang == 'vi')
                        msg = 'Đang xử lý quá trình nạp tiền';
                    else
                        msg = 'Topup card processing';
                      
                    $('.topup-success strong').text(msg);
                    $('.topup-success').show();

                    if(!hasResult)
                        checktransacion(code, lang);
                }
              }
            }).done(function(){
              // $('#img-loading').hide();
              $('.loading').css({'display':'none'});
              if(!hasResult)
                  checktransacion(code, lang);
            });
          }
        });


        $('a.btn-language').click(function() {
          var curr_lang = $('html').attr('lang');
          var lang = $(this).data().lang;
          if(lang == 'vi'){
            var next_lang = 'en';
          }
          else{
            var next_lang = 'vi';
          }
          $.ajax({
              url: '/change-language',
              type: 'post',
              data: {lang: next_lang},
              success: function(){
                  location.reload();
              }
          });
        });

        $('body').on('click','.open_gift_btn',function(e){
          e.preventDefault();
          
          // if($(window).width() > 414){
          //     var w = ($(window).width() - $("#voucherWrapper").width())/2;
          // }
          // else{
          //     var w = 0;
          // }
          $('.welcome_page').removeClass('active');
          $('.welcome_page').css({'right':'100%','left':'initial','transition':'0.3s'});
          $('.welcome_page .fixed_btn').css({'right':'100%','left':'initial','transition':'0.3s'});
          
          $('.main_page').addClass('active');
          $('.main_page').css({'right':'0px','left':'initial','transition':'0.3s','position':'relative'});
        });

      });


    $(".phone_number").keypress(validateNumber);
    function validateNumber(event) {
        event = window.event;
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
         || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    };

    function checkPermisionGeoLocation(){
      navigator.permissions.query({name:'geolocation'}).then(function(result) {
        if (result.state === 'denied') { 
          return false;
        } else{
            return true;
        }
      });
    }

    function FindNear()
    {      
      if (checkPermisionGeoLocation) {
        console.log('enable location')
        // HTML5/W3C Geolocation
        if ( navigator.geolocation )
        {
            navigator.geolocation.getCurrentPosition( UserLocation, errorCallback,{maximumAge:60000,enableHighAccuracy: true,timeout:10000});
        }
        else{
          ClosestLocation( 10.767661, 106.6999359, "254 Nguyen Cong Tru" );
        }   
      }else{
        alert('<?php echo trans("content.enable_location")?>');
      }
      
    }
    function errorCallback( error )
    { 
      console.log(error);
      if(error.code == 1){
        alert('<?php echo trans('content.enable_location')?>')
        return false;
      }
      
    }
    // Callback function for asynchronous call to HTML5 geolocation
    function UserLocation( position )
    {
        ClosestLocation( position.coords.latitude, position.coords.longitude, "This is my Location" );
    }

    // Convert Degress to Radians
    function Deg2Rad( deg ) {
       return deg * Math.PI / 180;
    }

    // Get Distance between two lat/lng points using the Haversine function
    // First published by Roger Sinnott in Sky & Telescope magazine in 1984 (“Virtues of the Haversine”)
    
    function Haversine( lat1, long1, lat2, long2 )
    {
        var R = 6372.8; // Earth Radius in Kilometers

        var dLat = Deg2Rad(lat2-lat1);  
        var dlong = Deg2Rad(long2-long1);  

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) * Math.sin(dlong/2) * Math.sin(dlong/2);  
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; 

        // Return Distance in Kilometers
        return d;
    }

    // Display a map centered at the nearest location with a marker and InfoWindow.
    function ClosestLocation( lat, long, title )
    {
        // find the closest location to the user's location
        var closest = 0;
        var mindist = 99999;

        for(var i = 0; i < locations.length; i++) 
        {
            // get the distance between user's location and this point
            var dist = Haversine( locations[ i ].lat, locations[ i ].long, lat, long );
            locations[i]['distance'] = dist;
        }
        if(typeFind == 'filterNearest'){
          locations = locations.filter(function(x) {
              return x.distance < 1;
          });
          locations.sort(function(a,b) { 
            return parseFloat(a.distance) - parseFloat(b.distance) ;
          });
        }
        else{
          locations.sort(function(a,b) { 
            return parseFloat(a.distance) - parseFloat(b.distance) ;
          });
        }
      
        // Create a Google coordinate object for the closest location
        var browser = navigator.userAgent;
         
        var ulClass="";
        $('.no-store').html('');
        console.log(typeFind)
        if(typeFind == 'filterBrand'){
          $(".brand-detail ul.list-store").empty();
          ulClass = '.brand-detail ul.list-store';
        }
        else if(typeFind == 'filterNearest'){
          $(".nearest-detail ul.list-store").empty();
          ulClass = '.nearest-detail ul.list-store';
        }
        $('.top-info span').text(locations.length + ' <?php echo trans('content.stores')?>');
        if(locations.length > 0){
          $.each(locations,function(index, storeObj){
            var store_name = storeObj.storeNm;
            var store_address = storeObj.storeAddr;
            var brand_name = storeObj.brandNm;
            var phoneText = "Số điện thoại: ";
            var addressText= "Địa chỉ: ";
            var distance = parseFloat(storeObj.distance).toFixed(2)+" KM";
            if(lang != 'vi'){
                phoneText = "Phone: ";
                addressText= "Address: ";
            }

            if(browser.match(/(iPhone|iPod|iPad)/)){
                var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
            } 
            else if(browser.match(/Android/)){
                var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
            }

            $(ulClass).append('<li><p class="goto-location">' + 
                brand_name + ' - ' + store_name +'<a href="'+link+'"></a></p>'+
                '<p class="address"><span>' +addressText+ store_address + '</span><br><span>'+
                (storeObj.phone != null && storeObj.phone != '' ? phoneText+storeObj.phone : '')+
                '</span><span class="distance">'+distance+'</span></p>'+
                '</li>'
            );
          }); 

        }
        else{
          $('.store-info').append('<div class="no-store"><p style="text-align:center">No store nearest here</p></div>');
        }
        
        
    }


    var loop = 30;
    var ind = 0;
    //loop for 30 times
    function checktransacion(code, lang){
      var hasResult = false;
      var request = {
          code:code
      };

      setTimeout(function() {
          if (ind <= loop) {
              ind++;

              $.ajax({
                  url: '<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/checkprocessing',//call lumen project
                  async: true,
                  crossDomain: true,
                  type: 'POST',
                  dataType: 'json',
                  contentType: "application/json; charset=utf-8",
                  headers: {
                      "Content-Type": "application/json"
                  },
                  data: JSON.stringify(request),

                  success: function (data) {
                      if(lang == 'vi'){
                          if (data.stt == 0) {
                              msg = 'Nạp tiền thành công';
                          }
                          else if (data.stt != 99 && data.stt != 100) {
                              msg = 'Nạp tiền thất bại';
                          }
                      }
                      else {
                          if (data.stt == 0) {
                              msg = 'Topup phone successfully';
                          }
                          else if (data.stt != 99 && data.stt != 100) {
                              msg = 'Topup phone failed';
                          }
                      }

                      if (data.stt == 0 || (data.stt != 99 && data.stt != 100)) {
                          $('.phontopup').html('').append('<p><b>' + msg + '</b></p>');
                          ind = loop + 1;//break the loop
                      }
                  },

                  error: function (xhr, ajaxOptions, thrownError) {
                      console.log(xhr.status);
                      console.log(ajaxOptions);
                      console.log(thrownError);
                  }
              });

              checktransacion(code);
          }
      }, 5000);
    }

    function getAllUrlParams(url) {
      // get query string from url (optional) or window
      var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
      // we'll store the parameters here
      var obj = {};
      // if query string exists
      if (queryString) {
        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];
        // split our query string into its component parts
        var arr = queryString.split('&');
        for (var i=0; i<arr.length; i++) {
          // separate the keys and the values
          var a = arr[i].split('=');
          // in case params look like: list[]=thing1&list[]=thing2
          var paramNum = undefined;
          var paramName = a[0].replace(/\[\d*\]/, function(v) {
            paramNum = v.slice(1,-1);
            return '';
          });

          // set parameter value (use 'true' if empty)
          var paramValue = typeof(a[1])==='undefined' ? true : a[1];
          // (optional) keep case consistent
          paramName = paramName.toLowerCase();
          paramValue = paramValue.toLowerCase();

          // if parameter name already exists
          if (obj[paramName]) {
            // convert value to array (if still string)
            if (typeof obj[paramName] === 'string') {
              obj[paramName] = [obj[paramName]];
            }
            // if no array index number specified...
            if (typeof paramNum === 'undefined') {
              // put the value on the end of the array
              obj[paramName].push(paramValue);
            }
            // if array index number specified...
            else {
              // put the value at that index number
              obj[paramName][paramNum] = paramValue;
            }
          }
          // if param name doesn't exist yet, set it
          else {
            obj[paramName] = paramValue;
          }
        }
      }

      return obj;
    }
    </script>
  </body>
  </html>