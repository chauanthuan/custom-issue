<?php
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class Unilever_Tresemme_PSServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views/Unilever_Tresemme_PS', 'Unilever_Tresemme_PS');
    }
    
}