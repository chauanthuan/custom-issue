<?php
namespace Dayone\Issuer;

class HSC_eGift_May {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\HSCServiceProvider');
        return 'HSC::hsc_egift_may';
    }

}