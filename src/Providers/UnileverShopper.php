<?php
namespace Dayone\Issuer;

class UnileverShopper {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\UnileverShopperServiceProvider');
        return 'UnileverShopper::index';
    }

}