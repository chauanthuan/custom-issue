<?php
namespace Dayone\Issuer;

class FeCredit_eGift {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\FeCreditServiceProvider');
        return 'FeCredit::fecredit_egift';
    }

}