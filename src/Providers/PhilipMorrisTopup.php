<?php
namespace Dayone\Issuer;

class PhilipMorris_Topup {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\PhilipMorrisServiceProvider');
        return 'PhilipMorris::topup';
    }

}