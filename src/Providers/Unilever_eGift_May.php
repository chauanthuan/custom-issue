<?php
namespace Dayone\Issuer;

class Unilever_eGift_May {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\UnilevereGiftMayServiceProvider');
        return 'Unilever_eGift_May::index';
    }

}