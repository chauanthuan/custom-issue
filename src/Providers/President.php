<?php
namespace Dayone\Issuer;

class President {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\PresidentServiceProvider');
        return 'President::index';
    }

}